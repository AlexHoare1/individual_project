import PIL.ImageDraw as ImageDraw
import PIL.Image as Image
from sympy import Point, pi, Polygon
from sympy.geometry import Line, Segment
import random
import os
import fcntl #READ/WRITE during multiprocessing
import multiprocessing
import re #REGEX LIBRARY
import math #Used to square root

#Distance weights
dw_doorbar = 1.0
dw_barbathroom = 0.9
dw_doorbath = 0.9
dw_doorstoreroom = 0.9
#Error space weights
ew_bar = 0.05
ew_bathroom = 0.05
ew_storeroom = 0.05
#Mean distance weights
mean_distance_weight = 0.1
#Area weights
aw = 0.0003

#solution_type = "EXHAUSTIVE"
solution_type = "HILL"
#solution_type = "SINGLE"
def main():
    #Remove the results.txt file if it already exists (for use in exhaustive)
    if solution_type == "EXHAUSTIVE":
        try:
            os.remove("results.txt")
        except Exception as e:
            pass
    ##Start and end coordinates of the footprint
    footprint_start_x = 100
    footprint_start_y = 100
    footprint_end_x = random.randint(500, 1700)
    footprint_end_y = random.randint(500, 900)

    ##X and Y coordinates of the centroid of the room
    footprint_centroid_x = (footprint_end_x + 100) * 0.5
    footprint_centroid_y = (footprint_end_y + 100) * 0.5

    ##Drawing the footprint to the screen
    footprint_type = random.randrange(0, 100) % 4
    if footprint_type == 0:
        points_footprint = drawFootprintRectangle(footprint_start_x, footprint_start_y, footprint_end_x, footprint_end_y)
    elif footprint_type == 1:
        points_footprint = drawFootprintL(footprint_start_x, footprint_start_y)
    elif footprint_type == 2:
        points_footprint = drawFootprintCross(100, 100)
    elif footprint_type == 3:
        points_footprint = drawFootprintT(100, 100)

    #Getting the area of the footprint and the Polygon
    footprinty = Polygon(*points_footprint)
    footprint_area = abs(Polygon(*points_footprint).area)

    ##Grabbing horizontal/vertical lines from footprint
    ordered_points = getLines(points_footprint)
    footprint_lines = getCartesianLines(ordered_points)

    ##Creating a random position for the door
    r = random.randrange(0, len(ordered_points) - 1)
    ##Drawing the door
    door_points, door_centre = drawDoor(ordered_points[r], ordered_points[r+1], 0.5, 10)

    #Find out the direction of the footprint wall, to draw an interior wall
    if ordered_points[r][0] == ordered_points[r + 1][0]:
        direction = False
    else:
        direction = True

    #Creating the walls within the footprint
    int_wall_num = 1 #Interior wall number
    interiorWallList = []
    for i in range(0, int_wall_num):
        r = random.randrange(0, len(ordered_points) - 1) ##used to be -2?
        d = float("0." + str(random.randrange(1, 9)))
        interiorWallList.append(drawWall(ordered_points[r], ordered_points[r + 1], d, direction, footprint_lines))

    ##Bathroom location and size
    bathroom_size = getBathroomSize(footprint_area)
    bar_size = round(((footprint_area * 0.00021777003) / 3) * 2)
    storeroom_size = bar_size

    if solution_type == "EXHAUSTIVE":
        ##Initialising variables to 0
        increment_1 = 0.0
        increment_2 = 0.0
        increment_3 = 0.0
        increment_size = 0.1
        for i in range(0, len(ordered_points) - 1):
            print("Looping")
            while increment_1 <= 1.0:
                for j in range(0, len(ordered_points) - 1):
                    while increment_2 <= 1.0:
                        for z in range(0, len(ordered_points) - 1):
                            while increment_3 <= 1.0:
                                p = multiprocessing.Process(target=calculateCost, args=(i, increment_1, bathroom_size, j, increment_2, bar_size, z, increment_3, storeroom_size, footprint_area, interiorWallList, footprinty, footprint_lines, door_centre, ordered_points, door_points))
                                p.start()
                                increment_3 = increment_3 + increment_size
                            increment_3 = 0.0
                        increment_2 = increment_2 + increment_size
                    increment_2 = 0.0
                increment_1 = increment_1 + increment_size
            increment_1 = 0.0

        bath_p, bar_p = exhaustive_GetPoints()
    elif solution_type == "HILL":
        #Creation to lists to store neighbours
        neighbour_list = []
        #Initial wall placements
        bathroom_wall = 0
        bar_wall = 1
        storeroom_wall = 2
        #Initial positions on walls
        storeroom_position = 0.0
        bathroom_position = 0.0
        bar_position = 0.0
        while True:
            print("Creating neighbours")
            ##Boolean variable to hold if local maximum is found
            found = False
            ##Create neighbours
            neighbours = createNeighbours(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position, ordered_points)
            ##Calculate cost of Initial
            initial_cost, bath_p, bar_p, storeroom_p = calculateCost(bathroom_wall, bathroom_position, bathroom_size, bar_wall, bar_position, bar_size, storeroom_wall, storeroom_position, storeroom_size, footprint_area, interiorWallList, footprinty, footprint_lines,  door_centre, ordered_points, door_points)
            ##Calculate cost of neighbours
            for n in neighbours:
                cost, n_bath_p, n_bar_p, n_storeroom_p = calculateCost(n.bathroom_wall, n.bathroom_position, bathroom_size, n.bar_wall, n.bar_position, bar_size, n.storeroom_wall, n.storeroom_position, storeroom_size, footprint_area, interiorWallList, footprinty, footprint_lines,  door_centre, ordered_points, door_points)
                n.cost = cost
            ##See if any neighbours smaller/greater than
            for n in neighbours:
                if n.cost > initial_cost:
                    found = True
                    bathroom_wall = n.bathroom_wall
                    bar_wall = n.bar_wall
                    bathroom_position = n.bathroom_position
                    bar_position = n.bar_position
                    storeroom_wall = n.storeroom_wall
                    storeroom_position = n.storeroom_position
                    initial_cost = n.cost
                    bath_p = n_bath_p
                    bar_p = n_bar_p
                    storeroom_p = n_storeroom_p
                    print("New best found")
            #If no new neighbours with a better cost have been found, leave
            if found == False:
                break
    elif solution_type == "SINGLE":
        cost, bath_p, bar_p, storeroom_p = calculateCost(0, 0.0, 174, 1, 0, bar_size, 2, 0.0, 50, footprint_area, interiorWallList, footprinty, footprint_lines, door_centre, ordered_points, door_points)
    #Drawing the rooms
    draw.polygon((bath_p), fill=(50, 150, 255))
    draw.polygon((bar_p), fill=(0, 255, 0))
    draw.polygon((storeroom_p), fill=(255, 0, 0))

    #Displaying the image
    image.show()

def drawStoreroom(start, end, position, size, lines, footprint, door_points, bar_lines):
    #Start point
    error = False
    wall_error = False
    D = Line(start, end)
    ##CALCULATING POINTS
    x = round(start[0] + ((end[0] - start[0]) * position))
    y = round(start[1] + ((end[1] - start[1]) * position))
    x2 = x + (D.direction.unit[0] * size)
    y2 = y + (D.direction.unit[1] * size)
    x3 = x2 + (D.direction.unit.rotate(-pi/2)[0] * size)
    y3 = y2 + (D.direction.unit.rotate(-pi/2)[1] * size)
    x4 = x3 + (D.direction.unit.rotate(-pi)[0] * size)
    y4 = y3 + (D.direction.unit.rotate(-pi)[1] * size)
    points = ((x, y), (x2, y2), (x3, y3), (x4, y4))

    #Checking if bathroom falls outside of footprint
    if D.direction.unit[0] == 0 and D.direction.unit[1] == 1:
        #DOWN
        if y2 > end[1]:
            error = True
    elif D.direction.unit[0] == 0 and D.direction.unit[1] == -1:
        #UP
        if y2 < end[1]:
            error = True
    elif D.direction.unit[0] == 1 and D.direction.unit[1] == 0:
        #RIGHT
        if x2 > end[0]:
            error = True
    elif D.direction.unit[0] == -1 and D.direction.unit[1] == 0:
        #LEFT
        if x2 < end[0]:
            error = True
    ##Checking if room is in front of door
    for door_point in door_points:
        if Polygon(*points).encloses_point(door_point):
            error = True

    #Checking if bathroom intersects with interior walls
    b_lines = []
    b_lines.append(Segment((x, y), (x2, y2)))
    b_lines.append(Segment((x2, y2), (x3, y3)))
    b_lines.append(Segment((x3, y3), (x4, y4)))
    b_lines.append(Segment((x4, y4), (x, y)))
    for b_line in b_lines:
        for j in range(0, len(lines)):
            p = b_line.intersection(lines[j])
            if p != []:
                return False, None, None
    try:
        for b_line in b_lines:
            for z in range(0, len(bar_lines)):
                p2 = b_line.intersection(bar_lines[z])
                if p2 != []:
                    return False, None, None
    except Exception as e:
        return False, None, None


    if error == False:
        return Polygon(*points), b_lines, points
    else:
        return False, None, None

def getBathroomSize(footprint_area):
    bathroom_size = 0
    #Facility sizes are in metres squared
    cubicle_size = 1.275
    sink_size = 0.0957
    urinal_size = 0.1105
    pixel_ratio = 0.00021777003
    metres_squared_area = round(footprint_area * pixel_ratio)
    patrons_number = round((metres_squared_area / 3) * 4)
    number_of_men = round(patrons_number * 0.75)
    number_of_women = round(patrons_number - number_of_men)
    #Calculating the size of the bathroom given the number of men
    if number_of_men < 75:
        bathroom_size = (2*sink_size) + (2 * urinal_size) + (1 * cubicle_size)
    elif number_of_men < 150:
        bathroom_size = (2*sink_size) + (3 * urinal_size) + (1 * cubicle_size)
    elif number_of_men < 225:
        bathroom_size = (3*sink_size) + (4 * urinal_size) + (2 * cubicle_size)
    elif number_of_men < 300:
        bathroom_size = (3*sink_size) + (5 * urinal_size) + (2 * cubicle_size)
    #Calculating the size of the bathroom given the number of number_of_women
    if number_of_women < 12:
        bathroom_size = (1*sink_size) + (1 * cubicle_size)
    elif number_of_women < 25:
        bathroom_size = (1*sink_size) + (2 * cubicle_size)
    elif number_of_women < 50:
        bathroom_size = (2*sink_size) + (3 * cubicle_size)
    elif number_of_women < 75:
        bathroom_size = (2*sink_size) + (4 * cubicle_size)
    elif number_of_women < 100:
        bathroom_size = (3*sink_size) + (5 * cubicle_size)
    #Converting back to pixels
    bathroom_size = bathroom_size / pixel_ratio
    #Square rooting to get dimensions
    bathroom_size = int(math.sqrt(bathroom_size))
    return bathroom_size

#Create the neighbours of the current code used for stochastic hill climbing
def createNeighbours(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position, ordered_points):
    neighbour_list = []
    #Bathroom Wall +1
    if bathroom_wall == len(ordered_points) - 2:
        #Create neighbour back at first wall
        neighbour_list.append(Neighbour(0, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position, 0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall + 1, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position, 0))
    #Bathroom Wall - 1
    if bathroom_wall == 0:
        neighbour_list.append(Neighbour(len(ordered_points) - 2, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall - 1, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position,0))

    #Bar Wall + 1
    if bar_wall == len(ordered_points) - 2:
        #Create neighbour back at first wall
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, 0, bar_position, storeroom_wall, storeroom_position,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall + 1, bar_position, storeroom_wall, storeroom_position,0))
    #Bar Wall - 1
    if bar_wall == 0:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, len(ordered_points) - 2, bar_position, storeroom_wall, storeroom_position, 0 ))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall - 1, bar_position, storeroom_wall, storeroom_position,0))

    #Storeroom Wall - 1
    if storeroom_wall == len(ordered_points) - 2:
        #Create neighbour back at first wall
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, 0, storeroom_position, 0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall + 1, storeroom_position, 0))
    #Storeroom Wall - 1
    if storeroom_wall == 0:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, len(ordered_points) - 2, storeroom_position, 0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall - 1, storeroom_position, 0))

    #Changing increment of bathroom
    if bathroom_position == 0.9:
        neighbour_list.append(Neighbour(bathroom_wall, 0.1, bar_wall, bar_position, storeroom_wall, storeroom_position,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position + 0.1, bar_wall, bar_position, storeroom_wall, storeroom_position,0))

    if bathroom_position == 0.0:
        neighbour_list.append(Neighbour(bathroom_wall, 0.9, bar_wall, bar_position, storeroom_wall, storeroom_position,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position - 0.1, bar_wall, bar_position, storeroom_wall, storeroom_position,0))

    #Changing increment of bar
    if bar_position == 0.9:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, 0.1, storeroom_wall, storeroom_position,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position + 0.1, storeroom_wall, storeroom_position,0))

    if bathroom_position == 0.0:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, 0.9, storeroom_wall, storeroom_position,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position - 0.1, storeroom_wall, storeroom_position,0))

    #Changing increment of storeroom
    if bar_position == 0.9:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, 0.1 ,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position + 0.1,0))

    if bathroom_position == 0.0:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, 0.9 ,0))
    else:
        neighbour_list.append(Neighbour(bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position - 0.1,0))

    return neighbour_list

#Neighbour class to hold all of the nodes
class Neighbour:
    def __init__(self, bathroom_wall, bathroom_position, bar_wall, bar_position, storeroom_wall, storeroom_position, cost):
        self.bathroom_wall = bathroom_wall
        self.bathroom_position = bathroom_position
        self.bar_wall = bar_wall
        self.bar_position = bar_position
        self.storeroom_wall = storeroom_wall
        self.storeroom_position = storeroom_position
        self.cost = cost

#Function to pull the data from the text file after the multiprocessing
def exhaustive_GetPoints():
    ##Opening text file to parse results
    highest = 0 #Variable used to store which room layout is the best
    f = open("results.txt", "r")
    for x in f:
        splitted = x.split("#")
        if splitted[0] > highest:

            highest = splitted[0]
            bath_p_string = splitted[1]
            bar_p_string = splitted[2]
            storeroom_p = splitted[3]

    #Pulling data from string for bathroom coordinates
    bath_p = bath_p_string.replace("Point2D", "")
    numbers = re.findall('\d+',bath_p)
    bath_p = [float(numbers[0]), float(numbers[1])]
    for i in range(2, len(numbers) - 1, 2):
        bath_p.insert(0, (float(numbers[i]), float(numbers[i+1])))

    #Pulling data from string for bathroom coordinates
    bar_p = bar_p_string.replace("Point2D", "")
    numbers = re.findall('\d+',bar_p)
    bar_p = [float(numbers[0]), float(numbers[1])]
    for i in range(2, len(numbers) - 1, 2):
        bar_p.insert(0, (float(numbers[i]), float(numbers[i+1])))

    #Pulling data from string for bathroom coordinates
    storeroom_p = storeroom_p_string.replace("Point2D", "")
    numbers = re.findall('\d+',storeroom_p)
    storeroom_p = [float(numbers[0]), float(numbers[1])]
    for i in range(2, len(numbers) - 1, 2):
        storeroom_p.insert(0, (float(numbers[i]), float(numbers[i+1])))
    #Return the points of the rooms
    return bath_p, bar_p, storeroom_p


#Function to position the rooms, and calculate their relative cost.
def calculateCost(bathroom_wall, bathroom_position, bathroom_size, bar_wall, bar_position, bar_size, storeroom_wall, storeroom_position, storeroom_size, footprint_area, interiorWallList, footprinty, footprint_lines,  door_centre, ordered_points, door_points):
    global mean_distance_weight, distance_weight, area_weight, error_weight, solution_type
    ##Creating the rooms themselves
    bath_poly, bath_lines, bath_points = drawBathroom(ordered_points[bathroom_wall], ordered_points[bathroom_wall+1], bathroom_position, bathroom_size, interiorWallList, footprinty, door_points)
    bar_poly, bar_lines, bar_points = drawBar(ordered_points[bar_wall], ordered_points[bar_wall+1], bar_position, bar_size, interiorWallList, footprinty, door_points)
    storeroom_poly, storeroom_lines, storeroom_points = drawStoreroom(ordered_points[storeroom_wall], ordered_points[storeroom_wall+1], storeroom_position, storeroom_size, interiorWallList, footprinty, door_points, bar_lines)
    ##COST FUNCTION
    if (bar_poly != False) and (bath_poly != False) and (storeroom_poly != False):
        ##Calculating distances between rooms
        distance_from_door = dw_doorbath * Segment(door_centre, bath_poly.centroid).length
        distance_between_bar_bathroom = dw_barbathroom * Segment(bath_poly.centroid, bar_poly.centroid).length
        distance_between_bar_door = dw_doorbar * Segment(door_centre,bar_poly.centroid).length ## set lower limit 4 congestion

        distance_between_storeroom_bar = dw_doorstoreroom * Segment(bar_poly.centroid, storeroom_poly.centroid).length
        ##Checking if there are spaces too small
        bathroom_invalid_space = ew_bathroom * abs(checkErrorSpace(footprint_lines, interiorWallList, bath_lines, 50))
        bar_invalid_space = ew_bar * abs(checkErrorSpace(footprint_lines, interiorWallList, bar_lines, 50))
        storeroom_invalid_space = ew_storeroom * abs(checkErrorSpace(footprint_lines, interiorWallList, storeroom_lines, 50))
        meanDistance = mean_distance_weight * getMeanDistanceFromDrinkingSpace(footprinty, bath_poly.centroid, bar_poly.centroid, storeroom_poly.centroid)
        area_costs = aw * (footprint_area - abs(bath_poly.area) - abs(bar_poly.area) - abs(storeroom_poly.area))
        distance_costs = distance_from_door + distance_between_bar_bathroom - distance_between_bar_door - distance_between_storeroom_bar
        error_space_costs = bathroom_invalid_space + bar_invalid_space + storeroom_invalid_space
        cost = area_costs + distance_costs - error_space_costs - meanDistance
        if solution_type == "EXHAUSTIVE":
            with open("results.txt", "a") as g:
                fcntl.flock(g, fcntl.LOCK_EX)
                g.write(str(cost) + "#" + str(bath_poly.vertices) + "#" + str(bar_poly.vertices) + "#" + str(storeroom_poly.vertices) + "\n")
                fcntl.flock(g, fcntl.LOCK_UN)
        elif solution_type == "HILL" or solution_type == "SINGLE":
            return cost, bath_points, bar_points, storeroom_points
    if solution_type == "HILL":
        return -2000, None, None, None
    return

#Function to get the mean distance from the drinking space
def getMeanDistanceFromDrinkingSpace(footprinty, bath_centroid, bar_centroid, storeroom_centroid):
    counter = 0
    total_distance = 0
    #Loops that go over every possible position in the footprint, step 100
    for x in range(100, 1700, 100):
        for y in range(100, 900, 100):
            #Checking if the point is within the footprint
            if footprinty.encloses_point((x, y)):
                #Calculate the distance and add it to the total
                total_distance = total_distance + Segment((x, y), (bath_centroid)).length
                total_distance = total_distance + Segment((x, y), (bar_centroid)).length
		total_distance = total_distance + Segment((x, y), (storeroom_centroid)).length
                counter = counter + 1
    #Return the average total mean distance
    return total_distance / counter ##THIS CAN CRASH DUE TO A BUG IN SYMPY

#Function to check if there are leftover spaces too small to be considered drinking space
def checkErrorSpace(footprint_lines, wall_lines, room_lines, minimum):
    total_area = 0
    #Checking all the lines of the room (bar, bath, etc)
    for r_line in room_lines:
        #Checking against the lines of the footprint
        for f_line in footprint_lines:
            if r_line.is_parallel(f_line):
                #Calculating the length of the perpendicular segment between wall and room
                s = f_line.perpendicular_segment(r_line.p1)
                #If the area is less than the threshold, add it to total_area
                if 0 < s.length < minimum:
                    total_area = total_area + (r_line.length * s.length)
        #Checking against the lines of the interior walls
        for w_line in wall_lines:
            if r_line.is_parallel(w_line):
                #Calculating the length of the perpendicular segment between wall and room
                s = w_line.perpendicular_segment(r_line.p1)
                #If the area is less than the threshold, add it to total_area
                if 0 < s.length < minimum:
                    total_area = total_area + (r_line.length * s.length)
    #Return the total amount of area rendered too small to be used
    return total_area

def drawBar(start, end, position, size, lines, footprint, door_points):
    #Start point
    error = False
    wall_error = False
    D = Line(start, end)
    ##CALCULATING POINTS
    x = round(start[0] + ((end[0] - start[0]) * position))
    y = round(start[1] + ((end[1] - start[1]) * position))
    x2 = x + (D.direction.unit[0] * (size * 2))
    y2 = y + (D.direction.unit[1] * (size * 2))
    x3 = x2 + (D.direction.unit.rotate(-pi/2)[0] * size)
    y3 = y2 + (D.direction.unit.rotate(-pi/2)[1] * size)
    x4 = x3 + (D.direction.unit.rotate(-pi)[0] * (size * 2))
    y4 = y3 + (D.direction.unit.rotate(-pi)[1] * (size * 2))
    points = ((x, y), (x2, y2), (x3, y3), (x4, y4))

    if D.direction.unit[0] == 0 and D.direction.unit[1] == 1:
        #DOWN
        if y2 > end[1]:
            error = True
    elif D.direction.unit[0] == 0 and D.direction.unit[1] == -1:
        #UP
        if y2 < end[1]:
            error = True
    elif D.direction.unit[0] == 1 and D.direction.unit[1] == 0:
        #RIGHT
        if x2 > end[0]:
            error = True
    elif D.direction.unit[0] == -1 and D.direction.unit[1] == 0:
        #LEFT
        if x2 < end[0]:
            error = True

    ##Checking if room is in front of door
    for door_point in door_points:
        if Polygon(*points).encloses_point(door_point):
            error = True

    #Checking if bathroom intersects with interior walls
    k_lines = []
    k_lines.append(Segment((x, y), (x2, y2)))
    k_lines.append(Segment((x2, y2), (x3, y3)))
    k_lines.append(Segment((x3, y3), (x4, y4)))
    k_lines.append(Segment((x4, y4), (x, y)))
    for kline in k_lines:
        for j in range(0, len(lines)):
            p = kline.intersection(lines[j])
            if p != []:
                return False, None, None
    if error == False:
        return Polygon(*points), k_lines, points
    else:
        return False, None, None


def drawBathroom(start, end, position, size, lines, footprint, door_points):
    #Start point
    error = False
    wall_error = False
    D = Line(start, end)
    ##CALCULATING POINTS
    x = round(start[0] + ((end[0] - start[0]) * position))
    y = round(start[1] + ((end[1] - start[1]) * position))
    x2 = x + (D.direction.unit[0] * size)
    y2 = y + (D.direction.unit[1] * size)
    x3 = x2 + (D.direction.unit.rotate(-pi/2)[0] * size)
    y3 = y2 + (D.direction.unit.rotate(-pi/2)[1] * size)
    x4 = x3 + (D.direction.unit.rotate(-pi)[0] * size)
    y4 = y3 + (D.direction.unit.rotate(-pi)[1] * size)
    points = ((x, y), (x2, y2), (x3, y3), (x4, y4))

    #Checking if bathroom falls outside of footprint
    if D.direction.unit[0] == 0 and D.direction.unit[1] == 1:
        #DOWN
        if y2 > end[1]:
            error = True
    elif D.direction.unit[0] == 0 and D.direction.unit[1] == -1:
        #UP
        if y2 < end[1]:
            error = True
    elif D.direction.unit[0] == 1 and D.direction.unit[1] == 0:
        #RIGHT
        if x2 > end[0]:
            error = True
    elif D.direction.unit[0] == -1 and D.direction.unit[1] == 0:
        #LEFT
        if x2 < end[0]:
            error = True
    ##Checking if room is in front of door
    for door_point in door_points:
        if Polygon(*points).encloses_point(door_point):
            error = True
    #Checking if bathroom intersects with interior walls
    b_lines = []
    b_lines.append(Segment((x, y), (x2, y2)))
    b_lines.append(Segment((x2, y2), (x3, y3)))
    b_lines.append(Segment((x3, y3), (x4, y4)))
    b_lines.append(Segment((x4, y4), (x, y)))
    for b_line in b_lines:
        for j in range(0, len(lines)):
            p = b_line.intersection(lines[j])
            if p != []:
                return False, None, None

    if error == False:
        return Polygon(*points), b_lines, points
    else:
        return False, None, None

def drawWall(start, end, position, direction, lines):
    found = False
    #Start point
    x = round(start[0] + ((end[0] - start[0]) * position))
    y = round(start[1] + ((end[1] - start[1]) * position))
    D = Line(start, end)
    dx = D.direction.unit.rotate(-pi/2)[0] * 500
    dy = D.direction.unit.rotate(-pi/2)[1] * 500
    L = Line((x, y), (x + dx, y + dy))

    for i in range(0, len(lines)):
        p = L.intersection(lines[i])
        if p != []:
            t = getT(x, y, x + dx, y + dy, lines[i].p1.x, lines[i].p1.y, lines[i].p2.x, lines[i].p2.y)
            u = getU(x, y, x + dx, y + dy, lines[i].p1.x, lines[i].p1.y, lines[i].p2.x, lines[i].p2.y)
            if t > 0 and (-1 <= u <= 0):
                intercepted_line = lines[i]
                break
    ##Assigning the values to a tuple
    p1 = (L.p1.x, L.p1.y)
    p2 = (p[0][0], p[0][1])
    p3 = (p1,) + (p2,)
    wallLine = Segment(p1, p2)
    draw.rectangle((p3), fill=(255,0,0))
    return wallLine

#Function to determine how far along a segment an intersection occurs
def getT(x1, y1, x2, y2, x3, y3, x4, y4) :
    t1 = (x1 - x3) * (y3 - y4)
    t2 = (y1 - y3) * (x3 - x4)
    b1 = (x1 - x2) * (y3 - y4)
    b2 = (y1 - y2) * (x3 - x4)
    return (t1 - t2)/(b1 - b2)

#Function to determine how far along a segment an intersection occurs
def getU(x1, y1, x2, y2, x3, y3, x4, y4):
    t1 = (x1 - x2) * (y1 - y3)
    t2 = (y1 - y2) * (x1 - x3)
    b1 = (x1 - x2) * (y3 - y4)
    b2 = (y1 - y2) * (x3 - x4)
    return (t1 - t2)/(b1 - b2)

#Draws the door to enter the footprint.
def drawDoor(start, end, position, size):
    #Getting the centre point of the door
    x = start[0] + ((end[0] - start[0]) * position)
    y = start[1] + ((end[1] - start[1]) * position)
    #Finding the direction of the wall it is placed on
    if (start[0] == end[0]):
        direction = True # VERTICAL
    else:
        direction = False # HORIZONTAL
    #Creating the shape of the door
    if direction:
        points = ((x - (size * 0.25), y - size), (x + (size * 0.25), y + size))
    else:
        points = ((x - size, y - (size * 0.25)), (x + size, y + (size * 0.25)))
    centre_point = (x, y)
    #Drawing the door
    draw.rectangle((points), fill=(0, 255, 0))
    #Returning the points of the door, and its centre point.
    return points, centre_point

#Draw a footprint as a rectangle, returning the points comprising the shape.
def drawFootprintRectangle(start_x, start_y, end_x, end_y):
    #Creation of the points
    points = ((start_x, start_y), (start_x, end_y), (end_x, end_y), (end_x, start_y))
    #Drawing of the footprint and returning the points
    draw.polygon((points), fill=(166, 166, 166))
    return points

#Draw a footprint in the shape of an L, returning the points comprising the shape.
def drawFootprintL(start_x, start_y):
    #Creation of the points
    point1 = (start_x, start_y)
    point2 = (start_x, point1[1] + random.randrange(150, 430))
    point3 = (point2[0] + random.randrange(150, 850), point2[1])
    point4 = (point3[0], point3[1] + random.randrange(150, 430))
    point5 = (point4[0] + random.randrange(150, 850), point4[1])
    point6 = (point5[0], start_y)
    #Compounding the tuples as a tuple of tuples.
    points = (point1,) + (point2,) + (point3,) + (point4,) + (point5,) + (point6,)
    #Drawing the shape and returning the points
    draw.polygon((points), fill=(166, 166, 166))
    return points

#Draw a footprint in the shape of a T, returning the points comprising the shape.
def drawFootprintT(start_x, start_y):
    #Creation of the points
    point1 = (start_x, start_y)
    point2 = (start_x, point1[1] + random.randrange(150, 430))
    point3 = (point2[0] + random.randrange(150, 566), point2[1])
    point4 = (point3[0], point3[1] + random.randrange(150, 430))
    point5 = (point4[0] + random.randrange(150, 566), point4[1])
    point6 = (point5[0], point3[1])
    point7 = (point6[0] + random.randrange(150, 566), point6[1])
    point8 = (point7[0], start_y)
    #Compounding the tuples as a tuple of tuples.
    points = (point1,) + (point2,) + (point3,) + (point4,) + (point5,) + (point6,) + (point7,) + (point8,)
    #Drawing the shape and returning the points
    draw.polygon((points), fill=(166, 166, 166))
    return points

#Draw a footprint in the shape of a cross, returning the points comprising the shape
def drawFootprintCross(start_x, start_y):
    #How large the sections will be
    extra_x = random.randrange(150, 400)
    extra_y = random.randrange(150, 250)
    #Creating the points
    n_start_x = start_x + extra_x
    n_start_y = start_y
    point1 = (n_start_x, n_start_y)
    point2 = (n_start_x, n_start_y + extra_y)
    point3 = (n_start_x - extra_x, point2[1])
    point4 = (point3[0], point3[1] + extra_y)
    point5 = (point4[0] + extra_x, point4[1])
    point6 = (point5[0], point5[1] + extra_y)
    point7 = (point6[0] + extra_x, point6[1])
    point8 = (point7[0], point7[1] - extra_y)
    point9 = (point8[0] + extra_x, point8[1])
    point10 = (point9[0], point9[1] - extra_y)
    point11 = (point10[0] - extra_x, point10[1])
    point12 = (point11[0], point11[1] - extra_y)
    #Compounding the tuples as a tuple of tuples.
    points = (point1,) + (point2,) + (point3,) + (point4,) + (point5,) + (point6,) + (point7,) + (point8,) + (point9,) + (point10,) + (point11,) + (point12,)
    #Drawing the shape
    draw.polygon((points), fill=(166, 166, 166))
    return points

#Function that returns an ordered list of points used to place rooms
def getLines(points):
    points = list(points)
    list_length = len(points)
    return_list = [points[0]]
    #Remove the first element as it is already in return_list
    points.remove(points[0])
    while True:
        #Checking if list has been ordered
        if len(return_list) == list_length:
            break
        else:
            for i in range(0, len(points)):
                #Checking if points on same x or y axis
                if return_list[len(return_list) - 1][0] == points[i][0]:
                    return_list.append(points[i])
                    points.remove(points[i])
                    break
                elif return_list[len(return_list) - 1][1] == points[i][1]:
                    return_list.append(points[i])
                    points.remove(points[i])
                    break
    #Add first element back to list to have complete circuit and return
    return_list.append(return_list[0])
    return return_list

#Return the points as a list of segments in Cartesian form
def getCartesianLines(points):
    lines_list = []
    #Segments are created and appended to the list
    for i in range(0, len(points) - 1):
        lines_list.append(Segment(points[i], points[i + 1]))
    return lines_list


image = Image.new("RGB", (1920, 980))
draw = ImageDraw.Draw(image)

main()


